# Get the location of the src directory
SRC_DIR  := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
ROOT_DIR := $(realpath $(dir $(SRC_DIR)))

# Include the global config makefile with custom configuration
GLOBAL_CONFIG_FILE := $(ROOT_DIR)/config.mk
CA_CONFIG_FILE     := ca.mk
-include $(GLOBAL_CONFIG_FILE)
-include $(CA_CONFIG_FILE)

# Directories and files
CERTS_DIR       := certs
CRL_DIR         := crl
CSR_DIR         := csr
PRIVATE_DIR     := private
INDEX_FILE      := index.txt
INDEX_ATTR_FILE := $(INDEX_FILE).attr
SERIAL_FILE     := serial
CRLNUMBER_FILE  := crlnumber

# CA files
CA_KEY_FILE     := $(PRIVATE_DIR)/ca.key.pem
$(CA_KEY_FILE)  :| $(PRIVATE_DIR)/
CA_CERT_FILE    := $(CERTS_DIR)/ca.cert.pem
$(CA_CERT_FILE) :| $(CERTS_DIR)/
CA_CSR_FILE     := $(CSR_DIR)/ca.csr.pem
$(CA_CSR_FILE)  :| $(CSR_DIR)/
CA_CRL_FILE     := $(CRL_DIR)/ca.crl.pem
$(CA_CRL_FILE)  :| $(CRL_DIR)/
# Make the key and certificate files precious so they aren't deleted
.PRECIOUS: $(CA_KEY_FILE) $(CA_CERT_FILE)
# Make the CSR a secondary file so it's not created just because it doesn't exist.
# Also avoids it being deleted so it is kept for reference.
.SECONDARY: $(CA_CSR_FILE)

# Path to the Root CA certificate file
ROOT_CA_CERT_FILE := $(ROOT_DIR)/$(CA_CERT_FILE)

export OPENSSL_CONF = openssl.cnf
KEY_NUMBITS = 4096

DEFAULT_COUNTRY_NAME ?= GB
DEFAULT_STATE_OR_PROVINCE_NAME ?= England
DEFAULT_ORGANIZATION_NAME ?= Alice Ltd.
DEFAULT_EMAIL_ADDRESS ?= $(USER)@$(shell hostname -f)

# Wildcard target to create directories
%/:
	mkdir -p $@
.PRECIOUS: %/

# Target to create private directory
$(PRIVATE_DIR)/:
	mkdir -m 700 $@

# Target to create index files
$(INDEX_FILE):
	touch $@

$(INDEX_ATTR_FILE):
	touch $@

# Target to create serial files
$(SERIAL_FILE):
	echo 1000 > $@

# Target to create crlnumber files
$(CRLNUMBER_FILE):
	echo 1000 > $@

# Target to create files from m4 templates
$(OPENSSL_CONF): $(SRC_DIR)/openssl.cnf.m4 $(wildcard $(GLOBAL_CONFIG_FILE)) $(wildcard $(CA_CONFIG_FILE)) $(MAKEFILE_LIST) | $(INDEX_FILE) $(SERIAL_FILE)
	m4 \
		--define __DIR__="$(abspath $(dir $@))" \
		--define __DEFAULT_COUNTRY_NAME__="$(DEFAULT_COUNTRY_NAME)" \
		--define __DEFAULT_STATE_OR_PROVINCE_NAME__="$(DEFAULT_STATE_OR_PROVINCE_NAME)" \
		--define __DEFAULT_LOCALITY_NAME__="$(DEFAULT_LOCALITY_NAME)" \
		--define __DEFAULT_ORGANIZATION_NAME__="$(DEFAULT_ORGANIZATION_NAME)" \
		--define __DEFAULT_ORGANIZATIONAL_UNIT_NAME__="$(DEFAULT_ORGANIZATIONAL_UNIT_NAME)" \
		--define __DEFAULT_EMAIL_ADDRESS__="$(DEFAULT_EMAIL_ADDRESS)" \
		$< > $@

# Generate RSA keys
$(PRIVATE_DIR)/%.key.pem: | $(PRIVATE_DIR)/
	openssl genrsa -aes256 -out $@ $(KEY_NUMBITS)
	chmod 400 $@
# Private keys are precious, don't delete them
.PRECIOUS: $(PRIVATE_DIR)/%.key.pem

# Targets to convert certificates to other formats
$(CERTS_DIR)/%.cert.der: $(CERTS_DIR)/%.cert.pem
	openssl x509 -outform der -in $< -out $@

$(CERTS_DIR)/%.cert.p7b: $(CERTS_DIR)/%.cert.pem $(CA_CERT_FILE) $(filter-out $(realpath $(CA_CERT_FILE)),$(ROOT_CA_CERT_FILE))
	openssl crl2pkcs7 -nocrl $(addprefix -certfile ,$^) -out $@

$(CERTS_DIR)/%.cert.p7c: $(CERTS_DIR)/%.cert.pem $(CA_CERT_FILE) $(filter-out $(realpath $(CA_CERT_FILE)),$(ROOT_CA_CERT_FILE)) $(CA_CRL_FILE)
	openssl crl2pkcs7 -in $(CA_CRL_FILE) $(addprefix -certfile ,$(filter-out $(CA_CRL_FILE),$^)) -out $@

# Target to output contents of CA certificate
.PHONY: show-ca
show-ca: $(CA_CERT_FILE)
	openssl x509 -noout -text -in $<

# Target to create the CA CRL
$(CA_CRL_FILE): $(CA_CERT_FILE) $(INDEX_FILE) | $(CRLNUMBER_FILE)
	openssl ca -gencrl $(OPENSSL_CA_ARGS) -out $@

.PHONY: ca-crl
ca-crl: $(CA_CRL_FILE)

# Target to remove the CA CRL
.PHONY: clean-ca-crl
clean-ca-crl:
	rm -f $(CA_CRL_FILE)

# Target to regenerate the CA CRL
.PHONY: regen-ca-crl
regen-ca-crl: clean-ca-crl ca-crl

# Target to output the contents of the CA CRL
.PHONY: show-ca-crl
show-ca-crl: $(CA_CRL_FILE)
	openssl crl -in $< -noout -text

.PHONY: clobber
clobber:
	rm -ir \
		$(CERTS_DIR)/ $(CRL_DIR)/ $(PRIVATE_DIR)/ \
		$(INDEX_FILE) $(INDEX_FILE).old \
		$(INDEX_ATTR_FILE) $(INDEX_ATTR_FILE).old \
		$(SERIAL_FILE) $(SERIAL_FILE).old \
		$(CRLNUMBER_FILE) $(CRLNUMBER_FILE).old \
		$(OPENSSL_CONF) ||:
