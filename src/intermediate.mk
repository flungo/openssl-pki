# Default target to make the Intermdiate CA
.PHONY: intermediate-ca
intermediate-ca:

include $(dir $(lastword $(MAKEFILE_LIST)))/openssl.mk

# Create the intermediate CA
intermediate-ca: $(CA_CERT_FILE)

$(CA_CERT_FILE): $(CA_CSR_FILE)
	# Avoid recursion loops
	test "$(MAKELEVEL)" -lt 2
	# The root CA needs to sign the cert for the intermediate CA
	echo $(MAKE) -C ../ $(notdir $(CURDIR))/$(CA_CERT_FILE)

# Create a signing request for the intermediate CA
$(CSR_DIR)/%.csr.pem: $(PRIVATE_DIR)/%.key.pem | $(OPENSSL_CONF) $(CSR_DIR)/
	openssl req -new -sha256 \
		-key $< -out $@
.PRECIOUS: $(CSR_DIR)/%.csr.pem
