ROOT_CA_DAYS        ?= 7300
INTERMDIATE_CA_DAYS ?= 3650

# Default target to make the Root CA
.PHONY: root-ca
root-ca:

include src/openssl.mk

# Create the root certificate
root-ca: $(CA_CERT_FILE)

$(CA_CERT_FILE): $(CA_KEY_FILE) | $(OPENSSL_CONF)
	openssl req $(OPENSSL_REQ_ARGS) \
		-key $(CA_KEY_FILE) \
		-new -x509 -days $(ROOT_CA_DAYS) -sha256 -extensions v3_ca \
		-out $@
	chmod 444 $@

# Create intermediate CA Makefiles
%/Makefile: %/
	echo "include ../$(notdir $(SRC_DIR))/intermediate.mk" > $@
.PRECIOUS: %/Makefile

# Create intermediate CA CSR
%/$(CA_CSR_FILE): %/Makefile
	$(MAKE) -C $(patsubst %/$(CA_CSR_FILE),%,$@)
.PRECIOUS: %/$(CA_CSR_FILE)

# Create intermediate CA Certificate
%/$(CA_CERT_FILE): %/$(CA_CSR_FILE) $(CA_CERT_FILE) | $(OPENSSL_CONF)
	openssl ca $(OPENSSL_CA_ARGS) -extensions v3_intermediate_ca \
		-days $(INTERMDIATE_CA_DAYS) -notext -md sha256 \
		-in $< -out $@
	chmod 444 $@
# Certificates are precious
.PRECIOUS: %/$(CA_CERT_FILE)

%-ca: %/$(CA_CERT_FILE)
	# Create a file as the wildcard target can't be made PHONY
	# Using symlink to cert file so that if it doesn't exist (but the symlink does) it will be created.
	ln -sf $< $@

# Clobber subordinate CAs
.PHONY: clobber-subordinates
clobber-subordinates:
ifneq ($(wildcard *-ca),)
	rm -ir \
		$(patsubst %-ca,%/,$(wildcard *-ca)) \
		$(wildcard *-ca) ||:
else
	@echo No subordinate CAs found
endif
